import { TEAMS,PROJECTS } from "./data/teams.js";


$(function () {

    let $app = $('.js_app');
    let $bot_view = $('.js_bot__view');
    let imageFolder = 'images/';



    function scrollTop(){
        $("html, body").animate({ scrollTop: 0 }, 0);
        return false
    }






    function screen__team(){
        let html = `
            <div class="team__view">
                <div class="top__bar">
                    <a href="javascript:void(0)" class="back__link js_btn_start_screen">
                        <img src="${imageFolder}icons/back.svg">
                    </a>

                </div>
                <div class="team__list">
        `;

        let addtitle = ''

        TEAMS.forEach(team => {
            let projectText,projectClass;

            if (team.projectId.length>1){
                projectText = 'проекты';
                projectClass = 'js__screen__projects___man';
                addtitle = `data-addtitle="${team.name}"`
            }else{
                projectText = 'проект';
                projectClass = 'js_show_projects';
            }


            html +=`
            <div class="team__item">
                <div class="team__item__top">
                    <div class="team__item__image">
                        <img src="${team.image}?1">
                    </div>
                    <div class="team__item__info">
                        <div class="team__item__name">${team.name}</div>
                        <div class="team__item__pos">${team.company}</div>
                    </div>
                </div>
                <div class="team__item__btns">
                    <a href="javascript:void(0)" data-class="js_btn_team_list" data-id="${team.id}"  class="btn_default btn_gray btn_x btn_x_1 js_meet_team">Познакомиться</a>
                    <a href="javascript:void(0)" data-class="js_btn_team_list" ${addtitle} data-idsprojects="${team.projectId}"  data-id="${team.id}" class="btn_default btn_red btn_x btn_x_1 btn_x_2 ${projectClass}">Посмотреть ${projectText}</a>
                </div>
            </div>`;
        });

        html += `
            </div>

            <div class='team__list_page__bottom'>
                <div class='team__list_page__bottom__text'>Если ты пока не определился с командой, но точно хочешь в МТС — просто отправь своё портфолио</div>

                <a href="javascript:void(0)" class="btn_default btn_red btn_x js__screen__form home__btn">Откликнуться</a>
            </div>

        </div>
        `;
        $bot_view.html(html)
        scrollTop()
        return false;
    }


    $(document).on('click', '.js_btn_start_screen', function(e){
        e.preventDefault();
        home__screen()
    })

    // page список команд
    $(document).on('click', '.js_btn_team_list', function(e){
        e.preventDefault();
        screen__team()
    })

    // page конкретной команды
    $(document).on('click', '.js_meet_team', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        screen__team__by_ID($(this).data('id'),backClass)
    })

    // page конкретного проекта
    $(document).on('click', '.js_show_projects', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        let id = $(this).data('idsprojects');
        console.log(id)
        screen__project__by_ID(id,backClass,$(this).data('id'))
    })

    // page о дизайне мтс
    $(document).on('click', '.js_screen_about_design', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        screen__about($(this).data('id'),backClass)
    })

    // page форма
    $(document).on('click', '.js__screen__form', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        screen__form($(this).data('id'),backClass)
    })

     // page all project
     $(document).on('click', '.js__screen__all_project', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        screen__allproject($(this).data('id'),backClass)
    })

    $(document).on('click', '.js__screen__projects___man', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        let ids = $(this).data('idsprojects');
        let addTitle = $(this).data('addtitle');

        console.log(addTitle)
        screen__projects___man(ids,backClass,$(this).data('id'),addTitle)
    })



     // page term
     $(document).on('click', '.js__screen__term', function(e){
        e.preventDefault();
        let backClass = $(this).data('class') ? $(this).data('class') : 'js_btn_team_list';
        screen__term($(this).data('id'),backClass)
    })




    $(document).on('submit', '.js_form', function(e){
        if ($(this)[0].checkValidity()) {
            let form = $(this);
            screen__thank();
            // $.ajax({
            //     type: "POST",
            //     url: ajax_url,
            //     data: formData,
            //     success: function () {
            //         form.trigger("reset");
            //         button.prop('disabled', false);
            //         location.href = form.find('.js__thank_url').val()
            //     }
            // });
        }
    });



    function home__screen(){
        let html = `<div class="home__view">
            <div class="home__view__inner">
                <div class="home__view__logo">
                    <img src="${imageFolder}logo.svg" alt="mts" class='js__screen__thank' >
                </div>
                <div class="home__view__image">
                    <img src="${imageFolder}bg_start.png" alt="Start screen">
                </div>
                <div class="home__view__info">
                    <div class="home__view__title">Здесь делают <span class="decore__text">дизайн.</span> И это правда круто
                    </div>
                    <a href="javascript:void(0)" class="btn_default btn_red btn_x js_btn_team_list home__btn">Выбери команду</a>
                </div>

                <div class="man__list__info man__list__info--home">
                    <a href="javascript:void(0)" class="man__info__item js__screen__all_project" data-class="js_btn_start_screen">
                        <div class="man__info__item__image">
                            <img src="${imageFolder}__content/project/1.jpg">
                        </div>
                        <div class="man__info__item__text">О проектах</div>
                    </a>
                    <a href="javascript:void(0)"  class="man__info__item js__screen__term"  data-class="js_btn_start_screen">
                        <div class="man__info__item__image">
                            <img src="${imageFolder}__content/project/2.jpg">
                        </div>
                        <div class="man__info__item__text">Об условиях</div>
                    </a>
                    <a href="javascript:void(0)" class="man__info__item js_screen_about_design" data-class="js_btn_start_screen">
                        <div class="man__info__item__image">
                            <img src="${imageFolder}__content/project/3.jpg">
                        </div>
                        <div class="man__info__item__text">О дизайне <br>в МТС</div>
                    </a>
                </div>


            </div>
        </div>`
        $bot_view.html(html)
        scrollTop()
        return false;
    }


    function screen__thank(){
        $bot_view.html(`
        <div class="thanks__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link js_btn_team_list">
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>
            <div class="thanks_block">
                <div class="thanks_block__image"><img src="${imageFolder}bg_thank.png"></div>
                <div class="title_x title_x--thanks ">Спасибо <br>за отклик! </div>
                <div class="text_thanks">Мы ушли изучать <br>твое портфолио и скоро<br> вернемся с ответом</div>
            </div>

            <div class="thanks_block__bottom">
                <a href="javascript:void(0)" class="btn_default btn_red btn_x js_btn_start_screen">Отлично!</a>
            </div>

        </div>`);
        scrollTop()
        return false;
    }


    function screen__term(id,classX){

        $bot_view.html(`
        <div class="about__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link ${classX}"  data-id="${id}">
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>

            <div class="title_x">У нас есть всё, <span class="decore__text">что нужно</span></div>
            <div class="project__text">
                <div class="default__text">
                <p><b>Мы заботимся и поддерживаем друг друга</b>, чтобы все в команде отлично себя чувствовали, получали удовольствия от работы, развивались и находили единомышленников </p>
                </div>
            </div>

            <div class="text__image__list">
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/1.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Выстроенные процессы</div>
                        <div class="text__item__text">Всё прозрачно и понятно, как новичку, так и опытному дизайнеру</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/2.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Дизайн-комьюнити</div>
                        <div class="text__item__text">Постоянная поддержка и обратная связь</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/3.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Техника</div>
                        <div class="text__item__text">В первый же день выдадим оборудование и софт для работы</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/4.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Плавный онбординг</div>
                        <div class="text__item__text">Никто не бросит вас в воду без спасательного жилета: вы постепенно погрузитесь в процессы, особенности экосистемы</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/5.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Бонусы</div>
                        <div class="text__item__text">ДМС с первого месяца работы, премии, скидки от МТС и партнеров, бесплатная корпоративная связь</div>

                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/6.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Развитие</div>
                        <div class="text__item__text">Оплатим обучение и участие в конференциях</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/1/7.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__title">Гибкость</div>
                        <div class="text__item__text">Возможность работы как удаленно, так и в офисе на Технопарке. Многие ребята из команды живут в разных городах — это не мешает нам работать продуктивно  и иногда приезжать друг  к другу</div>
                    </div>
                </div>
            </div>


            <div class="btn__bottom__block">
                <a href="javascript:void(0)" data-id='${id}' data-class='js__screen__term' class="btn_default btn_red btn_x js__screen__form">Хочу в команду</a>
                <a href="javascript:void(0)" data-id='${id}' data-class='js__screen__term' class="btn_default btn_gray btn_x js__screen__all_project">Посмотреть проекты</a>
            </div>
        </div>`);
        scrollTop()
        return false;
    }


    function screen__about(id,classX){
        $bot_view.html(`<div class="about__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link ${classX}"  data-id="${id}">
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>

            <div class="title_x">Дизайн <span class="decore__text">в МТС</span></div>
            <div class="project__text">
                <div class="default__text">
                    <p>Привет! Мы дизайн-команда МТС, у нас <b>больше 250 продуктов</b> и каждый день мы решаем сложные задачи.</p>
                    <p>Наша цель — улучшать жизнь людей с помощью удобных, полезных и приятных сервисов.</p>
                    <p>Мы строим дизайн-центричную компанию. Это означает, что дизайнер принимает решения и воплощает их. Он отвечает за продукт от начала и до конца.</p>
                    <p class="paragraph__title paragraph__title--full">Что делает нас лучше <br>и сильнее</p>
                </div>
            </div>

            <div class="text__image__list">
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/2/1.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__text">Своя дизайн-система с выделенной командой. Всё разложено по полочкам и проверено на разных продуктах, в том числе и в коде</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/2/2.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__text">Своя UX-лаборатория. Есть гипотеза? Быстро её проверим</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/2/3.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__text">Иллюстраторы, редакторы, аналитики и много людей с классными компетенциями, к которым можно обращаться за помощью</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/2/4.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__text">Гильдия дизайнеров.  Здесь мы обмениваемся опытом, приглашаем внешних экспертов и проводим вебинары. Чтобы всегда быть в курсе трендов, расти и развивать насмотренность, оплачиваем курсы и участие в конференциях</div>
                    </div>
                </div>
                <div class="text__image__item">
                    <div class="text__image__item__icon">
                        <img src="${imageFolder}icons/pack/2/5.png">
                    </div>
                    <div class="text__item__info">
                        <div class="text__item__text">Проводим MTS Desing Day, участвуем в Дизайн-выходных и Дизайн-просмотрах</div>
                    </div>
                </div>
            </div>


            <div class="btn__bottom__block">
                <a href="javascript:void(0)" data-id='${id}' data-class='js_screen_about_design' class="btn_default btn_red btn_x js__screen__form">Хочу в команду</a>
                <a href="javascript:void(0)" data-id='${id}' data-class='js_screen_about_design' class="btn_default btn_gray btn_x js__screen__all_project">Посмотреть проекты</a>
            </div>
        </div>`);
        scrollTop()
        return false;

    }





    function screen__allproject(id,classX){
        let allProjectHtml = '';
        PROJECTS.forEach(project => {
            allProjectHtml +=`
                <a data-idsprojects="${project.id}" data-class="js__screen__all_project" href="javascript:void(0)" class="projects__view__item js_show_projects">
                    <div class="projects__view__item__image">
                        <img src="${project.icon}">
                    </div>
                    <div class="projects__view__item__text">${project.name}</div>
                </a>`;
        })


        $bot_view.html(`
            <div class="projects__view">
                <div class="top__bar">
                    <a href="javascript:void(0)" class="back__link ${classX}" data-idsprojects="${id}">
                        <img src="${imageFolder}icons/back.svg">
                    </a>
                    <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                        <img src="${imageFolder}icons/home.svg">
                    </a>
                </div>
                <div class="title_x">Наши <br> <span class="decore__text">проекты</span> </div>
                <div class="projects__view__list">
                    ${allProjectHtml}
                </div>
                <a href="javascript:void(0)" class="btn_default btn_red btn_x js__screen__form">Хочу в команду</a>
            </div>`)

        scrollTop()
        return false;

    }

    function screen__projects___man(ids,classX,idBack,addtitle){
        let allProjectHtml = '';

        ids = ids.split(",").map(Number);
        console.log(ids)
        let projectsThis =[]
        let projects = PROJECTS.forEach(function(project) {
            ids.forEach(id => {
                if (project.id == id){
                    projectsThis.push(project)
                }
            });
        })


        console.log(projectsThis)

        projectsThis.forEach(project => {
                allProjectHtml +=`
                <a data-idsprojects="${project.id}" data-teamid='${idBack}' data-class="js_btn_team_list" href="javascript:void(0)" class="projects__view__item js_show_projects">
                    <div class="projects__view__item__image">
                        <img src="${project.icon}">
                    </div>
                    <div class="projects__view__item__text">${project.name}</div>
                </a>`;

        })


        $bot_view.html(`
            <div class="projects__view">
                <div class="top__bar">
                    <a href="javascript:void(0)" class="back__link ${classX}" data-id="${idBack}">
                        <img src="${imageFolder}icons/back.svg">
                    </a>
                    <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                        <img src="${imageFolder}icons/home.svg">
                    </a>
                </div>
                <div class="title_x">${addtitle} <br> <span class="decore__text">проекты</span></div>
                <div class="projects__view__list">
                    ${allProjectHtml}
                </div>
            </div>`)

        scrollTop()
        return false;

    }




    function screen__form(id,classX){
        let chooseTeamHtml = '<option value="--">Не выбрал</option>';
        PROJECTS.forEach(project => {
            if (localStorage.getItem('id') == project.id){
                chooseTeamHtml += `<option value="${project.name}" selected>${project.name}</option>`
            }else{
                chooseTeamHtml += `<option value="${project.name}">${project.name}</option>`
            }
        })

        let team = TEAMS.filter(team => {
            return team.id === id
        })
        team = team[0];



        $bot_view.html(
        `<div class="form__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link ${classX}" data-idsprojects="${id}"   data-id="${id}">
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>
            <div class="title_x">Отправить <br> <span class="decore__text">портфолио</span> </div>
            <div class="form_send_resume">
                <form action="javascript:void(0)" class="js_form">
                    <div class="field_x">
                        <input type="text" name="name" required placeholder="Имя Фамилия">
                    </div>
                    <div class="field_x">
                        <input type="tel" name="phone" required placeholder="Номер телефона">
                    </div>
                    <div class="field_x">
                        <input type="email" name="email" placeholder="Email">
                    </div>
                    <div class="field_x field_x__for_file js_field_x__for_file">
                        <input type="url" name="link" placeholder="Ссылка на портфолио" class='js_field_file field_file_xx'>
                        <label class="field_x__file js_field_x__file">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="512" height="512" x="0" y="0" viewBox="0 0 24 24" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g id="Layer_2" data-name="Layer 2"><path d="m6.739 21.95a5.749 5.749 0 0 1 -4.065-9.816l8.485-8.486a.75.75 0 1 1 1.061 1.061l-8.486 8.485a4.25 4.25 0 1 0 6.011 6.006l10.96-10.96a2.751 2.751 0 0 0 -3.89-3.89l-8.838 8.844a1.25 1.25 0 1 0 1.768 1.768l5.655-5.656a.75.75 0 1 1 1.061 1.06l-5.656 5.657a2.75 2.75 0 0 1 -3.89-3.889l8.84-8.834a4.25 4.25 0 1 1 6.011 6.01l-10.96 10.956a5.715 5.715 0 0 1 -4.067 1.684z" fill="#000000" data-original="#000000" class=""></path></g></g></svg>
                            <input type="file" name="file">
                        </label>

                    </div>
                    <div class="field_x field_x--select">
                        <select name="team" placeholder="Выбрать команду">
                            ${chooseTeamHtml}
                        </select>
                    </div>
                    <div class="field_x field_x--submiit">
                        <input type="submit" value="Отправить" class="btn_default btn_x btn_red">
                    </div>
                </form>
            </div>


        </div>`);

        scrollTop()
        return false;

    }



    $(document).on('change','.js_field_x__file input', function () {
        $('.js_field_file').val($(this)[0].files[0].name)

    })

    $(document).on('click','.js_man__view__image', function () {
        let videoUrl = $(this).data('video')
        let videoHtml = `
            <div class='video_modal js_video_modal'>
                <div class='video_modal__close js_video_modal__close'></div>
                <div class="video_modal__content">
                    <div class="video_modal__content__inner js_video_x">

                    </div>
                    <div class="video_modal__btn js_video_modal__btn"></div>
                    <div class="video__timeline js_video__timeline">
                        <div class="video__timeline__value js_video__timeline__value"></div>
                    </div>
                </div>
            </div>

        `;
        $bot_view.append(videoHtml)

        const video = document.createElement('video');
        video.src = videoUrl;
        video.preload = "auto";
        video.controls = false;
        video.classList.add('js_video')

        video.addEventListener('loadedmetadata', function() {
            video.play();
        });
        video.addEventListener("timeupdate", handleProgress);


        function handleProgress(){
            let progressPercentage = video.currentTime / video.duration * 100;
            console.log(progressPercentage)
        }

        document.querySelector('.js_video_x').appendChild(video);

    });


    $(document).on('click','.js_video_x', function () {
        let video = $('.js_video')[0];
        if ( video.paused || video.ended) {
            video.play();
          } else {
            video.pause();
          }
    })

    $(document).on('click','.js_video_modal__close', function () {
        $('.js_video_modal').addClass('hide')
        setTimeout(() => {
            $('.js_video_modal').remove()
        }, 250);

    })


    function screen__team__by_ID(id,classX){
        let team = TEAMS.filter(team => {
            return team.id === id
        })
        team = team[0];

        let projectClass;
        let addtitle = ''

        if (team.projectId.length>1){
            projectClass = 'js__screen__projects___man';
            addtitle = `data-addtitle="${team.name}"`
        }else{
            projectClass = 'js_show_projects';
        }


        localStorage.setItem('id', team.projectId[0]);


        let html = `
        <div class="man__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link ${classX}">
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>
            <div class="man__view__main">
                <div class="man__view__image js_man__view__image" data-video='${team.video}'>
                    <img src="${team.image}?1">
                </div>
                <div class="man__view__info">
                    <div class="man__view__title">${team.name}</div>
                    <div class="man__view__text">${team.company}</div>
                    <a href="javascript:void(0)" data-id='${id}' data-class='js_meet_team' class="js__screen__form btn_default btn_x btn_red man__view__btn">Выбрать эту команду</a>
                </div>
            </div>


            <div class="man__list__info">
                <a href="javascript:void(0)" class="man__info__item ${projectClass}"  ${addtitle} data-idsprojects="${team.projectId}" data-id="${team.id}"  data-class="js_meet_team">
                    <div class="man__info__item__image">
                        <img src="${imageFolder}__content/project/1.jpg">
                    </div>
                    <div class="man__info__item__text">О проектах</div>
                </a>
                <a href="javascript:void(0)"  class="man__info__item js__screen__term"  data-id="${id}" data-class="js_meet_team">
                    <div class="man__info__item__image">
                        <img src="${imageFolder}__content/project/2.jpg">
                    </div>
                    <div class="man__info__item__text">Об условиях</div>
                </a>
                <a href="javascript:void(0)" class="man__info__item js_screen_about_design" data-id="${id}" data-class="js_meet_team">
                    <div class="man__info__item__image">
                        <img src="${imageFolder}__content/project/3.jpg">
                    </div>
                    <div class="man__info__item__text">О дизайне <br>в МТС</div>
                </a>
            </div>
        </div>`;
        $bot_view.html(html)
        scrollTop()
        return false;
    }

    function screen__project__by_ID(id,classX,teamid){
        let project = PROJECTS.filter(project => {
            return project.id === id
        })
        project = project[0];

        let project_imagesHTML='';
        if (project.images){
            project_imagesHTML = '<div class="project__images">';

            project.images.forEach(image => {
                project_imagesHTML += `<img src="${image}">`;
            });

            project_imagesHTML += '</div>';

        }

        localStorage.setItem('id',project.id);
        let html = `



        <div class="project__view">
            <div class="top__bar">
                <a href="javascript:void(0)" class="back__link ${classX}" data-id='${teamid}'>
                    <img src="${imageFolder}icons/back.svg">
                </a>
                <a href="javascript:void(0)" class="home__link js_btn_start_screen">
                    <img src="${imageFolder}icons/home.svg">
                </a>
            </div>

            <div class="project__top">
                <img src="${project.icon}">
                <div class="title_x">${project.name}</div>
            </div>
            <div class="project__text">
                <div class="default__text">
                    ${project.text}
                    ${project_imagesHTML}
                </div>
            </div>



            <div class="btn__bottom__block">
                <a href="javascript:void(0)" data-id='${id}' data-idsprojects='${project.id}' data-class='js_show_projects' class="btn_default btn_red btn_x js__screen__form">Хочу в команду</a>
                <a href="javascript:void(0)" data-id='${id}' data-idsprojects='${project.id}' data-class='js_show_projects' class="btn_default btn_gray btn_x js__screen__all_project">Другие проекты</a>
            </div>



        </div>`;
        $bot_view.html(html)
        scrollTop()
        return false;
    }





    // start
    home__screen();
});